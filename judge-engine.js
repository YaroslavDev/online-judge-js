var streams = require('memory-streams')
var moment = require('moment')
var Docker = require('dockerode')
var JudgeUtils = require('./judge-util')

GLOBAL_TIME_LIMIT = 5000 // milliseconds
GLOBAL_MIN_MEMORY_LIMIT = 4 // megabytes
GLOBAL_MAX_MEMORY_LIMIT = 10 // megabytes

module.exports = {
	executeSubmission: executeSubmission
}

var docker = new Docker()

function executeSubmission(language, solutionSrcCode, testSrcCode, executeOpts, callback) {
	timeLimit = (executeOpts.timeLimit || GLOBAL_TIME_LIMIT)
	networkDisabled = (executeOpts.networkDisabled || true)
	memoryLimit = (executeOpts.memoryLimit || GLOBAL_MAX_MEMORY_LIMIT)
	memoryLimit = JudgeUtils.computeMemoryLimit(memoryLimit, GLOBAL_MIN_MEMORY_LIMIT, GLOBAL_MAX_MEMORY_LIMIT)
	createOptions = {
		Memory: memoryLimit,
		NetworkDisabled: networkDisabled
	}

	console.log("Begin executing submission written in %s", language)
	console.log("Source code:\n %s", solutionSrcCode)
	console.log("Test code:\n %s", testSrcCode)
	console.log("Time limit: %d milliseconds", timeLimit)
	console.log("Memory limit: %d bytes", memoryLimit)

	outputStream = new streams.WritableStream()
	runCmd = getRunCommand(language, solutionSrcCode, testSrcCode)
	container = runCmd[0]
	cmd = runCmd.slice(1)
	docker.run(container, cmd, outputStream, createOptions, function(err, data, container) {
		outputString = outputStream.toString()
		console.log("Container process data:\n %j", data)
		console.log("Container output:\n %s", outputString)

		if (data != null) {
			exitCode = data.StatusCode
		} else {
			exitCode = -1
		}
		outputLength = outputString.length
		outputString = outputString.substring(0, 200) + '\n...\n' + outputString.substring(outputLength - 100, outputLength)
		callback({
			exitCode: exitCode,
			status: exitCodeToStatus(exitCode),
			programOutput: outputString,
			endTime: moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ")
		})
	}).on('container', function(container) {
		setTimeout(function() {
			console.log("Killing container %s after %d millis", container.id, timeLimit)
			container.kill({}, function(err, data) {})
		}, timeLimit)
	})
}

function getRunCommand(language, solutionSourceCode, testSourcCode) {
	switch (language) {
		case "javascript":
			return [ 'nano/node.js', 'node', '-e', solutionSourceCode + '\n' + testSourcCode]
		case "python":
			return [ 'python', 'python', '-c', solutionSourceCode + '\n' + testSourcCode]
		case "ruby":
			return [ 'ruby', 'ruby', '-e', solutionSourceCode + ' ; ' + testSourcCode]
		default:
			throw "Unsupported language " + language
	}
}

function exitCodeToStatus(exitCode) {
	switch (exitCode) {
		case 0:
			return "Success"
		case 8:
			return "Test failed"
		case 137:
			return "Timeout"
		default:
			return "Failure"
	}
}
