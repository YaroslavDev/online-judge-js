var should = require('should')
var JudgeUtil = require('../judge-util')

describe('JudgeUtil', function() {
	describe('#computeMemoryLimit', function() {
		it('should return correct byte limit for megabyte limit between range', function() {
			megabytes = 4
			min = 4
			max = 10

			actualLimitBytes = JudgeUtil.computeMemoryLimit(megabytes, min, max)

			actualLimitBytes.should.equal(4 * 1024 * 1024)
		})

		it('should return correct value for megabytes below min', function() {
			megabytes = 2
			min = 4
			max = 10

			actualLimitBytes = JudgeUtil.computeMemoryLimit(megabytes, min, max)

			actualLimitBytes.should.equal(4 * 1024 * 1024)
		})

		it('should return correct value for megabytes greater max', function() {
			megabytes = 11
			min = 4
			max = 10

			actualLimitBytes = JudgeUtil.computeMemoryLimit(megabytes, min, max)

			actualLimitBytes.should.equal(10 * 1024 * 1024)
		})
	})
})