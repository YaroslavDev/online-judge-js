var should = require('should')
var JudgeEngine = require('../judge-engine')

describe('JudgeEngine', function() {
	describe('#executeSubmission', function() {
		it('should execute correct solution and return success status', function(done) {
			lang = "javascript"
			solution = "function fib(n) {\n if (n == 0) return 0\n else if (n == 1) return 1\n else return fib(n - 1) + fib(n - 2)\n}"
			test = "var assert = require('assert')\nassert(fib(7) == 13, \"fib(7) was not equal 13\")"

			JudgeEngine.executeSubmission(lang, solution, test, {}, function(response) {
				response.status.should.equal("Success")
				response.exitCode.should.equal(0)
				done()
			})
		})

		it('should execute incorrect solution and return stacktrace', function(done) {
			lang = "javascript"
			solution = "function fib(n) {\n if (n == 0) return 0\n else if (n == 1) return 1\n else return fib(n - 1) + fib(n - 1)\n}"
			test = "var assert = require('assert')\nassert(fib(7) == 13, \"fib(7) was not equal 13\")"

			JudgeEngine.executeSubmission(lang, solution, test, {}, function(response) {
				response.programOutput.should.containEql("fib(7) was not equal 13")
				response.exitCode.should.equal(8)
				response.status.should.equal("Test failed")
				done()
			})
		})

		it('should execute slow solution and return timeout status', function(done) {
			lang = "javascript"
			solution = "function fib(n) {\n if (n == 0) return 0\n else if (n == 1) return 1\n else return fib(n - 1) + fib(n - 2)\n}\n while(true){}\n"
			test = "var assert = require('assert')\nassert(fib(7) == 13, \"fib(7) was not equal 13\")"

			JudgeEngine.executeSubmission(lang, solution, test, {timeLimit: 1000}, function(response) {
				response.status.should.equal("Timeout")
				response.exitCode.should.equal(137)
				done()
			})
		})

		it('should execute solution that uses network and return getaddrinfo ENOTFOUND error', function(done) {
			lang = "javascript"
			solution = "http = require('http')\n http.get({host: 'google.com', path: '/'}, function(r) { r.on('data', function(d) { console.log(d.toString()) }); r.on('end', function(d) { console.log(d) }) })"
			test = "console.log('This should not appear')"

			JudgeEngine.executeSubmission(lang, solution, test, {networkDisabled: true}, function(response) {
				response.programOutput.should.containEql("ENOTFOUND")
				response.exitCode.should.equal(8)
				response.status.should.equal("Test failed")
				done()
			})
		})
	})
})