var util = require('util')
var MongoClient = require('mongodb').MongoClient;

MONGO_HOST = "localhost"
MONGO_PORT = "27017"
MONGO_USERNAME = "root"
MONGO_PASSWORD = "toor"
MONGO_DBNAME = "hackathon"
MONGO_URL = util.format('mongodb://%s:%s/%s', MONGO_HOST, MONGO_PORT, MONGO_DBNAME)

module.exports = {
    saveSubmission: saveSubmission,
    fetchSolution: fetchSolution
}

function saveSubmission(submission, callback) {
    console.log("Connecting to %s", MONGO_URL)
    MongoClient.connect(MONGO_URL, function(err, db) {
        if (err) {
            callback(err)
        } else {
            collection = db.collection('userSolutions')
            filter = {taskTitle: submission.taskTitle, username: submission.username}
            console.log("Fetching user solution with title %s, username %s", filter.taskTitle, filter.username)
            collection.findOne(filter).then(function(solution) {
                if (solution != null) {
                    console.log("Found user solution with title %s, username %s. Updating with new submission", filter.taskTitle, filter.username)
                    solution.submissions.push(submission)
                    collection.replaceOne(filter, solution, callback(solution))
                } else {
                    console.log("User solution with title %s, username %s NOT FOUND!", filter.taskTitle, filter.username)
                    callback(solution)
                }
                db.close()
            })
        }
    });
}

function fetchSolution(title, callback) {
    console.log("Connecting to %s", MONGO_URL)
    MongoClient.connect(MONGO_URL, function(err, db) {
        if (err) {
            console.log("Error while fetching: %s", err)
            callback(err)
        } else {
            db.collection('tasks').findOne({title: title}).then(function(task) {
                console.log("Fetched task: %j", task)
                callback(task)
                db.close()
            })
        }
    });
}