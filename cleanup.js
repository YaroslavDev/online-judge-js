var Docker = require('dockerode')

var docker = new Docker()

docker.listContainers({all: true}, function(err, containers) {
	containers.forEach(function(container) {
		console.log("Removing container %s", container.Id)
		docker.getContainer(container.Id).remove({}, function(err, data) {})
	})
})