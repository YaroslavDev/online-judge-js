module.exports = {
	computeMemoryLimit: computeMemoryLimit
}

function computeMemoryLimit(megabytes, min, max) {
	clamped = Math.min(Math.max(megabytes, min), max)
	bytes = Math.ceil(clamped * 1024 * 1024)
	return bytes
}