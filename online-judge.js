var http = require('http')
var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var JudgeEngine = require('./judge-engine')
var SumbissionService = require('./submission-service')

ONLINE_JUDGE_PORT = 3000

var app = express()
app.use(bodyParser.json())
app.use(cors())

app.get('/ping', function(req, res) {
	res.json("OK")
})

app.post('/submissions', function(req, res) {
	console.log("Received POST /submission request with body:\n %j", req.body)
	username = req.body.username
	language = req.body.language
	solutionSrcCode = req.body.solution
	SumbissionService.fetchSolution(req.body.taskName, function(solution) {
		tests = solution.tests
		test = tests[language]
		testSrcCode = test.sourceCode
		executeOpts = {
			timeLimit: test.timeLimit,
			memoryLimit: test.memoryLimit,
			NetworkDisabled: test.networkDisabled
		}
		JudgeEngine.executeSubmission(language, solutionSrcCode, testSrcCode, executeOpts, function(response) {
			res.json(response)
			submission = {
				taskTitle: req.body.taskName,
				username: username,
				language: language,
				sourceCode: solutionSrcCode,
				result: response.status,
				endTime: response.endTime
			}
			SumbissionService.saveSubmission(submission, function() {})
		})
	})
})

app.listen(ONLINE_JUDGE_PORT)
console.log('Online judge listening on *:%s', ONLINE_JUDGE_PORT);